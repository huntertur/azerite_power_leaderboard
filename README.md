# What is this?

This is a web app for displaying a leaderboard of characters in a World of
Warcraft guild based on their current Azerite Power level and experience. It is
inspired by an Artifact Power leaderboard I used during Legion that is now
defunct.

# Installation

Make sure you have Python 3.5+.

First, set up a virtual environment and install the dependencies:
```
$ python3 -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

Set up the database:
```
$ FLASK_APP=ap_leaderboard flask init-db
```

Create `instance/config.py` and place your Blizzard API keys in there:
```python
BLIZZARD_CLIENT_KEY = "your client id here"
BLIZZARD_CLIENT_SECRET = "your client secret here"
# You can also change the database location and maximum amount of connections
# DATABASE = "database/location.db"
# MAX_CONNECTIONS = 32
```

Then, you will need to create a WSGI file. How to set this up varies depending
on your web server setup, but the file should look something like this:
```python
import sys
path = "/path/to/azerite_power_leaderboard"
if path not in sys.path:
    sys.path.append(path)

from ap_leaderboard import app as application
```

