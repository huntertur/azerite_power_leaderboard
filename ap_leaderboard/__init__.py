import logging
import os

from flask import Flask, render_template, request, redirect, url_for

def create_app(test_config=None):
    logging.basicConfig(level=logging.INFO)

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        BLIZZARD_API_KEY=None,
        DATABASE=os.path.join(app.instance_path, "database.db"),
        AIOHTTP_PROXY=None,
    )

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    # ensure instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    from . import guild
    app.register_blueprint(guild.bp)
    app.add_url_rule("/", endpoint="guild.guild_page")

    @app.errorhandler(404)
    def page_not_found(error):
        """Display the 404 error page"""
        return render_template("404.html"), 404

    return app

app = create_app()
