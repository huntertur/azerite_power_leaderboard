DROP TABLE IF EXISTS guilds;
DROP TABLE IF EXISTS characters;
DROP TABLE IF EXISTS guild_characters;

CREATE TABLE guilds (
    name TEXT NOT NULL,
    realm TEXT NOT NULL,
    last_update INTEGER -- unix time
);

CREATE TABLE characters (
    name TEXT NOT NULL,
    realm TEXT NOT NULL,
    thumbnail TEXT, -- don't display it if there isn't one
    class TEXT,
    ap_level INTEGER NOT NULL,
    ap_xp INTEGER NOT NULL,
    ap_xp_remaining INTEGER NOT NULL
);

CREATE TABLE guild_characters (
    guild TEXT NOT NULL,
    character TEXT NOT NULL,
    realm TEXT NOT NULL,
    FOREIGN KEY(guild) REFERENCES guilds(name),
    FOREIGN KEY(character) REFERENCES characters(name)
);
