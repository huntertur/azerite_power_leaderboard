import json
import logging
import time

from base64 import b64encode
from datetime import datetime
from urllib.error import HTTPError
from urllib.request import Request, urlopen
from urllib.parse import urlencode

from flask import (
    Blueprint, g, redirect, request, render_template, url_for,
    current_app, abort,
)

from .db import get_db
from .util import url_format, download_parallel

LOGGER = logging.getLogger(__name__)

bp = Blueprint("guild", __name__)

# Blizzard's API returns a number for the class instead of a name
CLASSES = {
    1: "Warrior",
    2: "Paladin",
    3: "Hunter",
    4: "Rogue",
    5: "Priest",
    6: "Death Knight",
    7: "Shaman",
    8: "Mage",
    9: "Warlock",
    10: "Monk",
    11: "Druid",
    12: "Demon Hunter",
}

@bp.route("/")
def index_page():
    """Render the index page."""
    return render_template("index.html")

@bp.route("/redirect", methods=("GET",))
def redirect_from_form():
    """Redirect to the proper guild page from the form."""
    return redirect(url_for("guild.guild_page",
                            realm=request.args["realm"],
                            guild=request.args["guild"]))

@bp.route("/<realm>/<guild>")
def guild_page(realm=None, guild=None):
    """Render the leaderboard for a guild."""
    db = get_db()
    cursor = db.cursor()

    # Check if the guild is being tracked; if not, force an update.
    cursor.execute("""
        SELECT 1
        FROM guilds
        WHERE lower(name) = lower(?) AND lower(realm) = lower(?)
        LIMIT 1
    """, (guild, realm))
    if cursor.fetchone() is None:
        LOGGER.info("Adding <%s>-%s", guild, realm)
        try:
            pull_guild_info(realm, guild)
        except HTTPError:
            LOGGER.info("The guild does not exist.")
            abort(404)

    # If the guild is already tracked, check if it needs an update
    last_update = cursor.execute("""
        SELECT last_update
        FROM guilds
        WHERE lower(name) = lower(?) AND lower(realm) = lower(?)
        LIMIT 1
    """, (guild, realm)).fetchone()
    if time.time() - last_update["last_update"] >= 60 * 60: # one hour
        LOGGER.info("Updating <%s>-%s due to time", guild, realm)
        pull_guild_info(realm, guild)

    # Get actual captializations and last update time
    guild_info = cursor.execute("""
        SELECT name, realm, last_update
        FROM guilds
        WHERE lower(name) = lower(?) AND lower(realm) = lower(?)
        LIMIT 1
    """, (guild, realm)).fetchone()

    characters = cursor.execute("""
        SELECT name, realm, thumbnail, class, ap_level, ap_xp, ap_xp_remaining
        FROM characters
        WHERE name IN (
            SELECT character
            FROM guild_characters
            WHERE guild = ? AND realm = ?
        ) AND realm = ?
        ORDER BY ap_level DESC, ap_xp DESC
    """, (guild_info["name"], guild_info["realm"], guild_info["realm"]))

    return render_template("guild.html", characters=characters,
                           guild_info=guild_info, datetime=datetime)

def pull_guild_info(realm, guild):
    """Pull the guild's leaderboard information from the API.

    This includes:
     - Guild members
     - Character info
    
    This function is run whenever a new guild is added and when an
    hour has passed since the last guild update.
    """
    db = get_db()
    cursor = db.cursor()

    client_id = current_app.config["BLIZZARD_CLIENT_ID"]
    client_secret = current_app.config["BLIZZARD_CLIENT_SECRET"]
    # see https://develop.battle.net/documentation/guides/using-oauth/
    #     client-credentials-flow
    data = urlencode({"grant_type": "client_credentials"}).encode()
    token_url = Request("https://us.battle.net/oauth/token", data=data)
    auth = b64encode("{}:{}".format(client_id, client_secret).encode("ascii"))
    token_url.add_header("Authorization",
                         "Basic {}".format(auth.decode("ascii")))
    key = json.loads(urlopen(token_url).read().decode())["access_token"]

    guild_api = url_format("https://us.api.blizzard.com/wow/guild/{}/{}"
                           "?fields=members&locale=en_US&access_token={}",
                           realm, guild, key)

    char_api = "https://us.api.blizzard.com/wow/character/{}/{}" \
                "?fields=items&locale=en_US&access_token={}" \

    # Get all characters in the guild and convert to object
    with urlopen(guild_api) as response:
        # It seems like json.loads swallows the HTTPError exception that gets
        # thrown, so `guild_json` can't be inlined.
        guild_json = response.read().decode("utf8")
        guild_info = json.loads(guild_json)

    guild_name = guild_info["name"] # get correct capitalization
    guild_realm = guild_info["realm"] # get correct capitalization

    cursor.execute("""
        DELETE FROM guilds WHERE name = ? AND realm = ?
    """, (guild_name, guild_realm))
    cursor.execute("""
        INSERT INTO guilds VALUES (?, ?, strftime('%s', 'now'))
    """, (guild_name, guild_realm))

    char_names = [] # to look up all members
    for entry in guild_info["members"]:
        if entry["character"]["level"] >= 111: # must have Heart
            char_names.append(entry["character"]["name"])

    # 404 if there is nobody with a Heart of Azeroth
    if not char_names:
        abort(404)

    # Request all character data from the API
    # Create the URLs for the character API requests
    char_urls = []
    for name in char_names:
        char_urls.append(url_format(char_api, realm, name, key))

    # Parallel download
    chars_json = download_parallel(char_urls)
    chars = []
    for obj in chars_json:
        if obj is not None:
            chars.append(json.loads(obj))

    for char in chars:
        if "status" in char and char["status"] == "nok":
            # This character could not be looked up for some reason, so do not
            # add it to the leaderboard since information cannot be found
            continue

        if "neck" not in char["items"] \
                or "azeriteItem" not in char["items"]["neck"]:
            continue

        cursor.execute("""
            DELETE FROM characters WHERE name = ? AND realm = ?
        """, (char["name"], char["realm"]))
        cursor.execute("""
            INSERT INTO characters VALUES (?,?,?,?,?,?,?)
        """, (
            char["name"],
            char["realm"],
            char["thumbnail"],
            CLASSES.get(char["class"]),
            char["items"]["neck"]["azeriteItem"]["azeriteLevel"],
            char["items"]["neck"]["azeriteItem"]["azeriteExperience"],
            char["items"]["neck"]["azeriteItem"]["azeriteExperienceRemaining"],
        ))

        cursor.execute("""
            DELETE FROM guild_characters
            WHERE guild = ? AND character = ? AND realm = ?
        """, (guild_name, char["name"], char["realm"]))
        cursor.execute("INSERT INTO guild_characters VALUES (?,?,?)",
                   (guild_name, char["name"], guild_realm))

    db.commit()
