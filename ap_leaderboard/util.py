import asyncio
import time

import aiohttp

from urllib.parse import quote

from flask import current_app

def url_format(base, *args, **kwargs):
    """Like str.format, but the arguments are quoted for a URL.

    Slashes are also quoted as well.
    """
    new_args = [quote(arg, safe='') for arg in args \
                if isinstance(arg, (str, bytes))]

    new_kwargs = {key: quote(value, safe='') for key, value in kwargs.items() \
                  if isinstance(value, (str, bytes))}

    return str.format(base, *new_args, **new_kwargs)

def download_parallel(urls):
    """Request many URLs in parallel.

    This prevents the issue of using a loop with urlopen, where the requests
    are made one at a time, which takes much longer.
    """
    # Note: I, huntertur (hukaulaba), the author of this code, have no idea
    # what I am doing with asynchronous programming. This is probably wrong for
    # several reasons, but as far as I am aware, this is the only way to do
    # this without making this function async, and the function that calls
    # this async, and so on until making create_app async, which I am pretty
    # sure I cannot do.
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    results = loop.run_until_complete(_download_multiple_urls(urls))
    loop.close()
    return results

async def _download_multiple_urls(urls):
    # This needs to be seperate from download_parallel because it contains
    # async code.

    # Blizzard's API allows 100 requests per second. If there are more than 100
    # URLs, they are split into batches of 100 to be processed at a time before
    # waiting for 1 second to reset the limit.
    async with aiohttp.ClientSession() as session:
        coroutines = [_download_url(url, session) for url in urls]
        batches = [coroutines[i:i+100] for i in range(0, len(urls), 100)]

        results = []

        for batch in batches:
            results.extend(await asyncio.gather(*batch))
            await asyncio.sleep(1)

        return results

async def _download_url(url, session):
    async with session.get(
                url,
                proxy=current_app.config["AIOHTTP_PROXY"]
            ) as response:
        return await response.text()
